package com.example.test;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;


public class TestObservable {
	Observable sut;
	Object value;
	
	@Before
	public void init() {
		value = new Integer(2);
		sut = new Observable(value);
	}
	
	@Test
	public void test_attach() {		
		Observer observer = new Observer();
		sut.attach(observer);
	}

	@Test
	public void test_update_called() {
		Observer observer = mock(Observer.class);
		sut.attach(observer);	
		
		sut.notify_status();
		
		verify(observer).update(value);
	}
	
	@Test
	public void test_two_observers() {
		Observer observer1 = mock(Observer.class);
		Observer observer2 = mock(Observer.class);
		sut.attach(observer1);
		sut.attach(observer2);

		sut.notify_status();
		
		verify(observer1).update(value);
		verify(observer2).update(value);
	}
}
