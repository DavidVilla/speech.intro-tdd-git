package com.example.test;

import java.util.ArrayList;
import java.util.Iterator;

public class Observable {
	private ArrayList<Observer> observers;
	private Object status;
	
	public Observable(Object initial) {
		observers = new ArrayList<Observer>(1);
		status = initial;
	}
	
	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void notify_status() {
		Iterator<Observer> it = observers.iterator();
		while (it.hasNext())
			it.next().update(status);
	}
	
	public Object get_status() {
		return status;
	}
}
