package com.example;

import java.util.ArrayList;

public class Stack {
	private ArrayList<Integer> data;
	
	public Stack() {
		data = new ArrayList<Integer>();
	}
	
	public int size() {
		return data.size();
	}

	public void push(int i) {
		data.add(i);
	}

	public int top() {
		return data.get(size() - 1);
	}

	public int pop() throws Empty {
		if (size() == 0) {
			throw new Empty();
		}
		
		int retval = top();
		data.remove(size() - 1);
		return retval;
	}

}
