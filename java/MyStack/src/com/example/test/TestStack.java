package com.example.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.example.Empty;
import com.example.Stack;

public class TestStack {
	private Stack sut;
	
	@Before
	public void create() {
		sut = new Stack();
	}
		
	@Test
	public void has_size_zero() {
		assertEquals(sut.size(), 0);
	}
	
	@Test
	public void push_value_set_size_to_one() {
		sut.push(1);
		assertEquals(sut.size(), 1);
	}
	
	@Test
	public void push_and_pop_set_size_to_zero() {
		sut.push(1);
		try {
			sut.pop();
		} catch (Empty e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(sut.size(), 0);
	}	
	
	@Test
	public void returns_last_pushed_values_as_top() {
		sut.push(1);
		assertEquals(1, sut.top());
	}
	
	@Test
	public void returns_last_pushed_with_pop() {
		sut.push(1);
		try {
			assertEquals(1, sut.pop());
		} catch (Empty e) {
			fail();
		}
	}
	
	@Test
	public void raise_empty_on_pop() {
		try {
			sut.pop();
			fail();
		} catch (Empty e) {
		}
	}
}
